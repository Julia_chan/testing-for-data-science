from sklearn.base import BaseEstimator, TransformerMixin

class ToNumericNans_Right(BaseEstimator, TransformerMixin):
    """Convert columns with all NULLs to float.
    It's a known issue that columns with all NULLs will have NoneType when processed with spark. We have to force their conversion to float
    (numpy.nan)
    """
    
    def fit(self, X, y=None):
        self.null_columns = X.columns[X.isnull().all()].tolist()
        return self
    
    def transform(self, X):
        df = X.copy()
        df.loc[:, self.null_columns] = df.loc[:, self.null_columns].astype(float)
        return df